<?php 
session_start();
if(isset($_POST['params'])){
$params = $_POST['params'];

// if($params['code'] == "update"){
//     $id = $params['data']['userid'];
//     $name = $params['data']['name'];
//     $sname = $params['data']['sname'];
//     $address = $params['data']['address'];
// }
// }else{
//     echo $params['code'] = 'adduser';
}
?>

<script type="text/javascript">
$(document).ready(function(){
    var params = {};
    params = <?php echo json_encode($params);?>;
    //console.log(params)
    if(params.code == 'adduser'){
        $("#name").removeAttr('value');
        $("#surname").removeAttr('value');
        $("#address").removeAttr('value');
        $("#ModalLongTitle").text('ADD USER');
    $("#addformbtn").click(function () {        //adduser
                name = $('#name').val()

                if(name == '') {
                    $("#name").focus();
                    return false;
                }
                
                sname = $('#surname').val()
                address = $('#address').val()
                params.code = 'insertdata';
                params.data = {
                    name,
                    sname,
                    address
                }
                //console.log(params)
                $.ajax({
                    url: 'query.php',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        'params': params
                    },
                    error: function (xhr, ajaxOption, thrownError) {
                        console.log(xhr.responseText);
                    },

                    success: function (data, status) {
                        //console.log(data, status)
                        switch (data.code) {
                            case 0: //insert success
                                alert(data.msg)
                                console.log(data)
                                $('#name').val('')
                                $('#surname').val('')
                                $('#address').val('')

                                $("#viewuser").click();
                                $('#new_modal').modal('hide');
                                break;

                            case 1: // error something
                                alert(data.msg)
                                break;
                        }
                    }

                })
            })
        }

        if(params.code == 'update'){            //Edit Modal Begin
            //$('#userid').removeClass('d-none')
            $("#ModalLongTitle").text('EDIT USER');
            $('#userid').attr('value',params.data.userid)
            $('#name').attr('value',params.data.name)
            $('#surname').attr('value',params.data.sname)
            $('#address').attr('value',params.data.address)

            $("#addformbtn").on('click',function(){            //edit user  save button
                new_name = $("#name").val()
                
                new_surname = $("#surname").val()
                new_address = $("#address").val()
                userid = $("#userid").val()
                params.newdat = {
                    userid,new_name,new_surname,new_address
                }
                params.data =''

                
                console.log(params)
                $.ajax({
                    url:'query.php',
                    type:'POST',
                    dataType:'json',
                    data:{'params':params},
                    error: function (xhr, ajaxOption, thrownError) {
                        console.log(xhr.responseText);
                    },
                    success:function(data,status){
                        console.log(data)

                        switch (data.code){
                            case 1:
                            alert(data.msg)
                            break;

                            case 3:
                            $('#new_modal').modal('toggle')
                            $("#viewuser").click();

                            // udcont = "<td class='userid' >" 
                            //                 +data.ud.user_id +"</td> <td class='username' >" 
                            //                 + data.ud.name + "</td> <td class='usersname' >" 
                            //                 +data.ud.surname +"</td><td class='useraddress' >" 
                            //                 + data.ud.address +"</td>";
                            // tr_row.remove().children(td .userid,td .username,td.usersname,td .useraddress)
                            // tr_row.append(udcont)

                            //params.ud = udcont;
                            break;
                        }
                    }
                })
            })
        }

        if(params.code=='deluser'){
            $("#ModalLongTitle").text('DELETE USER');
            $("div .modal-body").empty()
            $("div .modal-footer").empty()
            $("div .modal-body").append(delmodal)
            $("#delagree").on('click',function(){
                console.log(params)
                $.ajax({
                    url:'query.php',
                    type:'POST',
                    datatype:'json',
                    data:{
                        'params':params
                    },
                    error: function (xhr, ajaxOption, thrownError) {
                        console.log(xhr.responseText);
                    },
                    success:function(data,status){
                        console.log(data)
                        switch(data.code){
                        case 1:
                            alert(data.msg)
                            break;
                        case 4: 
                            alert(data.msg)
                        $('#new_modal').modal('toggle')
                        tr_row.hide();
                        break;
                        }
                        
                    }
                })
            })

            $("#delcancel").on('click',function(){
                $('#new_modal').modal('toggle')
                
            })
        }

        if(params.code =='login'){
            $("#ModalLongTitle").text('LOGIN');
            $("div .modal-body").empty()
            $("div .modal-body").append(loginmodal)
            $("#addformbtn").text('LOGIN')

                $("#addformbtn").on('click',function(){
                    var usrname = $("#username").val()
                    var pass = $("#password").val()
                    params.usrname = usrname;
                    params.pass = pass
                   // console.log(params)
                    $.ajax({
                        url:'query.php',
                        type:'POST',
                        datatype:'json',
                        data:{
                            'params':params
                        },
                        error: function (xhr, ajaxOption, thrownError) {
                        console.log(xhr.responseText);
                        },
                        success:function(data){
                            console.log(data)
                            switch(data.code){
                                case 1:
                                alert(data.msg)
                                break;
                                case 6:
                                alert(data.msg)
                                $('#new_modal').modal('toggle')
                                location.reload()
                                break;
                            }
                        }
                    })
                })
        }

        if(params.code =='regis'){
            $("#ModalLongTitle").text('REGISTER');
            $("div .modal-body").empty()
            $("div .modal-body").append(regismodal)
            $("#addformbtn").text('REGISTER')

                $("#addformbtn").on('click',function(){
                    var usrname = $("#username").val()
                    var pass = $("#password").val()
                    var email = $("#email").val()
                    params.usrname = usrname;
                    params.pass = pass
                    params.email = email
                    console.log(params)
                    $.ajax({
                        url:'query.php',
                        type:'POST',
                        datatype:'json',
                        data:{
                            'params':params
                        },
                        error: function (xhr, ajaxOption, thrownError) {
                        console.log(xhr.responseText);
                        },
                        success:function(data){
                            console.log(data)
                            switch(data.code){
                                case 1:
                                alert(data.msg)
                                break;
                                case 5:
                                alert(data.msg)
                                $('#new_modal').modal('toggle')
                                params.code = 'login'
                                $('#new_modal').modal('toggle')
                                break;
                            }
                        }
                    })
                })
        }
})
    
</script>


<html>
<div class="modal fade" id="new_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header ">
                <h5 class="modal-title mx-auto" id="ModalLongTitle">Modal title xxxxxxxxxxxxxxxxxx</h5>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"> -->
                    <!-- <span aria-hidden="true">&times;</span> -->
                <!-- </button> -->
            </div>
            <div class="modal-body text-center">
                <input type="text" id="userid" class="d-none" value="" placeholder="" disabled><br><br>
                <input type="text" id="name" required="required" value=""
                    placeholder="Input Name"><br><br>
                <input type="text" id="surname" required="required" value=""
                    placeholder="Input Surname"><br><br>
                <input type="text" id="address" required="required" value=""
                    placeholder="Input Address"><br><br>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="addformbtn">SAVE</button>
            </div>
        </div>
    </div>
</div>


<script>
var delmodal ="<div class='row align-items-center'>"+    
"<div class=' col-md-12 col-lg-12 test-center'>"
+"<h4>Warning!! Confirm To Delete User</h4></div>"+
"<div class=' col-md-12 col-lg-12 justify-content-between'>"
+"<button type='button' class='btn btn-lg btn-danger pull-left' id='delagree'>YES</button>"
+"<button type='button' class='btn btn-lg btn-warning pull-right' id='delcancel'>NO</button></div></div>";


var loginmodal = 
"<div class='form-group row'>"+
    "<label for='username' class='col-sm-3 col-form-label'>username</label>"+
    "<div class='col-sm-9 '><input id='username' class='form-control' data-validation='required' type='text' name='username'></div>"+
       
    "<label for='password' class='col-sm-3 col-form-label'>password</label>"+
    "<div class='col-sm-9'><input id='password' data-validation='required' class='form-control' type='password' name='password'></div>"+
    
"</div>"
        ;

var regismodal = 
"<div class='form-group row'>"+
    "<label for='username' class='col-sm-3 col-form-label'>username</label>"+
    "<div class='col-sm-9 '><input id='username' class='form-control' data-validation='required' type='text' name='username'></div>"+
       
    "<label for='password' class='col-sm-3 col-form-label'>password</label>"+
    "<div class='col-sm-9'><input id='password' data-validation='required' class='form-control' type='password' name='password'></div>"+
    
    "<label for='email' class='col-sm-3 col-form-label'>E-Mail</label>"+
    "<div class='col-sm-9'><input id='email' data-validation='required' class='form-control' type='email' name='email'></div>"+
"</div>"
        ;
    </script>
</html>