<?php session_start()?> 
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> -->
    <title>USER DATA</title>

    <style>
        .viewhead {
            background-color: rgba(70, 80, 90, .76);
        }

        body {
            font-family: sans-serif
        }
    </style>
</head>

<body>
    <script type="text/javascript">
        var tr_row;
        $(document).ready(function() {
            //$("#view").hide();
            // $("#adduserform").hide();
            // $("#adduser").click(function () {
            //     $("#adduserform").show();
            params = {};
            $("#viewuser").on('click', function() {
                params['code'] = 'getdata';
                //console.log(params)

                $.ajax({
                    url: 'query.php',
                    type: 'POST',
                    datatype: 'json',
                    data: {
                        'params': params
                    },
                    error: function(xhr, ajaxOption, thrownError) {
                        console.log(xhr.responseText);
                    },
                    success: function(data, status) {
                        console.log(data)

                        switch (data.code) {
                            case 1:
                                alert(data.msg)
                                break;

                            case 2:
                                $('#view tbody').empty();
                                var content = '';

                                $.each(data.item, function(i, dat) {


                                    content = content +
                                        "<tr class='record_user' data-index = " + i + "><td class='userid' >" +
                                        dat.user_id + "</td> <td class='username' >" +
                                        dat.name + "</td> <td class='usersname' >" +
                                        dat.surname + "</td><td class='useraddress' >" +
                                        dat.address + "</td><td><button type='button' class='edit btn btn-success'>Edit</button></td>" +
                                        "<td><button type='button' class='btn btn-danger delbtn' >Delete</button></td></tr>"; //'data-name='"+dat.name+"' data-id='"+dat.user_id+" data-sname='"+dat.surname+" data-address='"+dat.address+"

                                })
                                $("div .showdata").removeClass("invisible");
                                $("#view > tbody").append(content)
                                $("div .showend").removeClass("invisible");
                                params.code = '';

                                break;

                        }

                        $(".edit").click(function() { //ปุ่มedit อยู่ในปุ่ม view user อีกที 
                            tr_row = $(this).closest('tr');
                            name = $(this).parents().children(".username").text() //ขุดจากตารางที่โชว์
                            userid = $(this).parents().children(".userid").text()
                            sname = $(this).parents().children(".usersname").text()
                            address = $(this).parents().children(".useraddress").text()
                            index = $(this).parents('.record_user').attr('data-index')
                            params.data = {
                                name,
                                userid,
                                sname,
                                address
                            }
                            params.code = 'update';
                            console.log(params)
                            console.log(index)

                            $.ajax({
                                url: 'modal.php',
                                type: 'POST',
                                datatype: 'json',
                                data: {
                                    'params': params
                                },
                                success: function(data) {

                                    $('#landing-modal').html(data) //open modal
                                    $('#new_modal').modal('toggle') //open modal
                                    console.log(data.ud)


                                }
                            });

                        });


                        $(".delbtn").on('click', function() {
                            tr_row = $(this).closest("tr");

                            //delete
                            userid = $(this).parents().children(".userid").text()
                            params.code = 'deluser'
                            params.data = userid
                            // id = $("#delbtn").val()
                            // params.data = id 
                            console.log(params)
                            $.ajax({
                                url: 'modal.php',
                                type: 'POST',
                                datatype: 'json',
                                data: {
                                    'params': params
                                },
                                success: function(data) {
                                    //console.log(data)
                                    $('#landing-modal').html(data)
                                    $('#new_modal').modal('toggle')
                                }
                            })
                        })
                    }
                })
            })

            $(".add").click(function() {
                //var par =$(this).attr('data-id')   //เรียกจากที่ประกาศ
                //    var name =$(this).attr('data-name')   //เรียกจากที่ประกาศ
                //     $('#editid').val(par)
                //     $('#editname').val(name)
                params.code = 'adduser'
                $.ajax({
                    url: 'modal.php',
                    type: 'POST',
                    datatype: 'json',
                    data: {
                        'params': params
                    },
                    success: function(data) {
                        //console.log(data)
                        $('#landing-modal').html(data) //open modal
                        $('#new_modal').modal() //open modal
                    }
                });

            });
            $(".ending").on('click', function() {
                $("div .showdata").addClass('invisible');
            })
            $("#loginn").click(function(){
                params.code = 'login';
                $.ajax({
                    url:'modal.php',
                    datatype:'json',
                    type:'POST',
                    data:{
                        'params':params
                    },
                    success:function(data){
                        $('#landing-modal').html(data)
                        $('#new_modal').modal('toggle')
                    }
                })
            })

            $("#register").click(function(){
                params.code = 'regis';
                $.ajax({
                    url:'modal.php',
                    datatype:'json',
                    type:'POST',
                    data:{
                        'params':params
                    },
                    success:function(data){
                        $('#landing-modal').html(data)
                        $('#new_modal').modal('toggle')
                    }
                })
            })

            $("#logout").click(function(){
                <?php 
                    //session_destroy();    
                ?>
            })
        })
    </script>
    <div id="landing-modal"></div>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
            <a class="navbar-brand" href="#">Navbar</a>


            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    
                </ul>
                <ul class="navbar-nav ml-auto">
                    <?php if(isset($_SESSION['add'])){?>
                        <li class="nav-item ">
                        <div><button type="button" class="btn btn-outline-primary " id="viewuser">Show</button></div>

                    </li>
                    <li class="nav-item ">
                        <div><button type="button" class="btn btn-outline-success add ">Add User</button></div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Dropdown
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="" id='logout'>log out</a>
                        </div>
                    </li>
                <?php }else{ ?>
                    
                    <li class="nav-item">
                        <a class="nav-link login"  id="loginn" tabindex="-1">Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link register" id="register" tabindex="-1">Register</a>
                    </li>
                <?php } ?>
                </ul>
            </div>
        </div>
    </nav>
    <!-- <div class="container">
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel" style="height:400px !important;">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="https://images.unsplash.com/photo-1476514525535-07fb3b4ae5f1?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1280&h=400&q=60" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="https://images.unsplash.com/photo-1463288889890-a56b2853c40f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1280&h=400&q=80" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="https://images.unsplash.com/photo-1491331606314-1d15535360fa?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1280&h=400&q=80" class="d-block w-100" alt="...">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    </div> -->
    <div class="container">
        <!--ส่วนshow -->
        <div class="row showdata invisible">

            <div class="col-12">
                <br>
                <table class="table table-striped table-hover text-center " id="view">

                    <thead>
                        <tr class="text-center">
                            <th colspan="6">
                                <h1>USER DATA</h1>
                            </th>
                        </tr>
                        <tr>
                            <th scope="col">USER ID</th>
                            <th scope="col">NAME</th>
                            <th scope="col">SURNAME</th>
                            <th scope="col">ADDRESS</th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>

                </table>
                <div class="invisible showend">
                    <button type="button" class="btn btn-success ending">Close</button>
                </div>
            </div>
        </div>




        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
        </script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
        </script>

</body>

</html>